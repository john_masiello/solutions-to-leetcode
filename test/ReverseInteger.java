import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.Map.Entry;

public class ReverseInteger {
	
	private Solution solution;

	@Before
	public void setUp() throws Exception {
		solution = new Solution_2();
	}

	@Test
	public void test() {
		assertEquals(321, solution.reverse(123));
	}
	
	@Test
	public void test2() {
		assertEquals(-321, solution.reverse(-123));
	}
	
	@Test
	public void test3() {
		assertEquals(21, solution.reverse(120));
	}
	
	@Test
	public void test4() {
		assertEquals(0, solution.reverse(2147483647));
	}
	
	@Test
	public void test5() {
		assertEquals(0, solution.reverse(-2147483648));
	}
	
	@Test
	public void test6() {
		assertEquals(0, solution.reverse(1534236469));
	}
	
	@Test
	public void test7() {
		assertEquals(-2143847412, solution.reverse(-2147483412));
	}
	
	public static abstract class Solution {
		 public abstract int reverse(int x);
	}
	
	public static class Solution_0 extends Solution {

		@Override
		public int reverse(int x) {
			int result = 0;
			while (x != 0) {
				result *= 10;
				result += x % 10;
				x /= 10;
			}
			return result;
		}
		
	}
	
	public static class Solution_1 extends Solution {

		@Override
		public int reverse(int x) {
			int result = 0;
			boolean shouldCheckOverflow = x > 0 ? x > 999_999_999 :
				x < -999_999_999;
			
			int[] integerMaxOrMin = x > 0 ? new int[] {2,1,4,7,4,8,3,6,4,7} :
				new int[] {2,1,4,7,4,8,3,6,4,8};
			
			int digit, absDigit;
			int place = 0;
			
			while (x != 0) {
				result *= 10;
				digit = x % 10;
				if (shouldCheckOverflow) {
					absDigit = abs(digit);
					
					if (absDigit > integerMaxOrMin[place]) {
						// overflow
						return 0;
						
					} else if (absDigit < integerMaxOrMin[place])  {
						shouldCheckOverflow = false;
						
					} else {
						place++;
					}
				}
				result += digit;
				x /= 10;
			}
			return result;
		}
		
		public int abs(int x) {
			return Math.abs(x);
		}
		
	}
	
	public static class Solution_2 extends Solution {

		@Override
		public int reverse(int x) {
			int result = 0;
			
			while (x != 0) {
				try {
					result =  Math.addExact(Math.multiplyExact(result, 10), x % 10);
				
				} catch (ArithmeticException ignore) {
					return 0;
				}
				x /= 10;
			}
			return result;
		}
	}
}