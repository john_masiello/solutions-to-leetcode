import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.Map.Entry;

public class TwoSum {
	
	private Solution solution;

	@Before
	public void setUp() throws Exception {
		solution = new Solution_0();
	}

	@Test
	public void test() {
		int[] input = new int[] {2,7,11,15};
		int target = 9;
		int[] result = solution.twoSum(input, target);
		int[] expected = new int[] {0, 1};
		assertArrayEquals("Incorrect solution", expected, result);
	}
	
	@Test
	public void test2() {
		int[] input = new int[] {3,2,4};
		int target = 6;
		int[] result = solution.twoSum(input, target);
		int[] expected = new int[] {1, 2};
		assertArrayEquals("Incorrect solution", expected, result);
	}
	
	@Test
	public void test3() {
		int[] input = new int[] {4, 4};
		int target = 8;
		int[] result = solution.twoSum(input, target);
		int[] expected = new int[] {0, 1};
		assertArrayEquals("Incorrect solution", expected, result);
	}
	
	public static abstract class Solution {
		 public abstract int[] twoSum(int[] nums, int target);
	}
	
	public static class Solution_0 extends Solution {
	    public int[] twoSum(int[] nums, int target) {
	        Map<Integer, Integer> indexes = new HashMap<>();
	        int length = nums.length;
	        for (int i = 0; i < length; i++) {
	            indexes.put(nums[i], i);
	        }
	        Integer index;
	        for (int i = 0; i < length; i++) {
	            index = indexes.get(target - nums[i]);
	            if (index != null && index != i) {
	                int[] result = new int[2];
	                result[0] = i;
	                result[1] = index;
	                return result;
	            }
	        }
	        return new int[0];
	    }
	}
	
	// Assumes lots of duplicates
	public static class Solution_1 extends Solution {
	    public int[] twoSum(int[] nums, int target) {
	        Map<Integer, IndexPair> indexes = new HashMap<>();
	        int length = nums.length;
	        IndexPair indexPair;
	        
	        for (int i = 0; i < length; i++) {
	        	final int index = i;
	        	indexPair = indexes.computeIfAbsent(nums[i], $ -> new IndexPair(index));
	        	indexPair.setIndex(index);
	        }
	        
	        for (Entry<Integer, IndexPair> pair : indexes.entrySet()) {
	        	indexPair = indexes.get(target - pair.getKey());
	            if (indexPair != null && 
	            		indexPair.getLatestIndex() != pair.getValue().getEarliestIndex()) {
	            	
	                int[] result = new int[2];
	                result[0] = pair.getValue().getEarliestIndex();
	                result[1] = indexPair.getLatestIndex();
	                return result;
	            }
	        }
	        return new int[0];
	    }
	}
	
	// Assumes lots of duplicates
	public static class Solution_2 extends Solution {
	    public int[] twoSum(int[] nums, int target) {
	        Map<Integer, IndexPair> indexes = new HashMap<>();
	        int length = nums.length;
	        int earliestIndex;
	        IndexPair indexPair;
	        
	        for (int i = 0; i < length; i++) {
	        	final int index = i;
	        	indexPair = indexes.computeIfAbsent(nums[i], $ -> new IndexPair());
	        	indexPair.setIndex(index);
	        }
	        
	        for (int num : indexes.keySet()) {
	        	indexPair = indexes.get(target - num);
	        	if (indexPair == null)
	        		continue;
	        	earliestIndex = indexes.get(num).getEarliestIndex();
	            if (indexPair.getLatestIndex() != earliestIndex) {
	            	
	                int[] result = new int[2];
	                result[0] = earliestIndex;
	                result[1] = indexPair.getLatestIndex();
	                return result;
	            }
	        }
	        return new int[0];
	    }
	}
	
	public static class IndexPair {
		private Integer oldIndex;
		private Integer newIndex;
		
		public IndexPair() {
		}

		public IndexPair(Integer index) {
			this.oldIndex = index;
			this.newIndex = index;
		}

		public void setIndex(int index) {
			if (oldIndex == null)
				oldIndex = index;
			else
				newIndex = index;
		}
		
		public Integer getEarliestIndex() {
			return oldIndex;
		}
		
		public Integer getLatestIndex() {
			return newIndex == null ? oldIndex : newIndex;
		}
	}
}